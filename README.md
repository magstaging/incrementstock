# README 

product stock does not get decremented anymore when an order is placed

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/IncrementStock when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

